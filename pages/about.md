---
layout: default
title: À propos
date: 2022-03-24
edit: 2023-08-07
---

# {{ title }}

## Un jardin qui pousse tout seul

Salut ! Bienvenue dans [mon jardin](https://maggieappleton.com/garden-history "Maggie Appleton : A Brief History & Ethos of the Digital Garden"). Si vous êtes déjà venu chez moi, vous savez que ma philosophie concernant les plantes est proche du *live and let live*{lang="en"}… bref, attendez-vous à des coins pas débroussaillés, des hautes herbes, et des arbres tordus en train de pousser n’importe comment !  

Le site tourne sous [Eleventy](https://11ty.dev/ "Site officiel d'Eleventy"), et le thème est un fork d'[Eleventy Garden](https://github.com/binyamin/eleventy-garden "Dépôt git d'Eleventy Garden"), je le personalise au fur et à mesure pour ajouter des fonctionnalités de mon ancien thème, [Jekyll Garden](https://jekyll-garden.github.io/ "Dépôt git de Jekyll Garden"). J’essaie aussi de le rendre accessible, on apprends en faisant !  
Tout le code du site est trouvable sur [ce git](https://codeberg.org/yazae101/yazae.garden "Dépôt git de ce site").

## Le coin à champignons
Il s’agit aussi, un peu, de mon jardin secret. Si vous avez trouvé ce site sans que je vous ai filé le lien, bravo, mais, euh, ça reste entre nous, hein? C’est comme les bons coins à champignons, ça se partage qu’entre ami·es 😉

De manière plus claire, partagez les pages et le site si vous le trouvez utile, mais je préfèrerais que vous ne le liez pas à mes autres identités d'internet, et que vous ne l'indexez pas publiquement sur des moteurs de recherche ou [archive.org](https://web.archive.org/ "Wayback archive"). Le robot.txt est pas là pour rien ! 😑

<!-- ## La ~~squatteuse~~{aria-hidden="true"} locataire du jardin -->
## La ~~squatteuse~~ locataire du jardin
Soyons honnêtes 5 minutes, y’a peu de chance que ce site serve à beaucoup d’autres gens que moi, mais bon : ayant quelques problèmes avec la notion de propriété, je ne suis que locataire (ou squatteuse ?) ici.  

J’y habite, j’y écris, mais ça s’arrête là : vous pouvez réutiliser mon contenu comme vous le voulez, tout est sous double licence [CC BY-SA](https://creativecommons.org/licenses/by-sa/2.0/deed.fr "Creative Commons : CC BY-SA") et [Hot Chocolate Licence](https://git.vadia.eu.org/Vadia/HotChocolateLicence "Hot Chocolate Licence"){lang="en"}.
Vous pouvez choisir celle qui vous conviens le plus, mais en gros, faites-en ce que vous voulez tant que c'est pas capitaliste, et que vous vendez pas des [NFT à partir de mon contenu](https://framablog.org/2021/09/23/comment-se-faire-10-000-boules-sur-le-dos-dun-artiste-libre/ "David Revoy : N’achetez pas les NFT Dream Cats") 😅

Ah, sinon, vous vouliez en savoir plus sur moi ? Allez voir [par ici !](/notes/yazae/)

---  

Dernière édition le {{ edit | date: "%d-%m-%Y" }}.
