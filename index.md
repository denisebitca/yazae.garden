---
layout: default
title: Home
---
# Yazae<span class="emphase-alt">.</span>
*Un blog perso où je code, j’écris et assemble des p’tits bouts d’trucs.*

Bonjour ! Vous voici sur mon blog ! Il est un peu en chantier pour le moment, mais y’a déjà quelques pages fonctionnelles ! Allez voir mes [notes](/notes/) pour commencer à visiter mon jardin pas si secret <span aria-hidden="true">;)</span><span class="v-hidden">Émoji clin d’œil</span>