# Changelog

Tous les changements notables de ce projet seront documentés dans ce fichier.

Le format est basé sur [Tenez un Changelog](https://keepachangelog.com/fr/). Ce projet adhère au [versionnage sémantique](https://semver.org/spec/v2.0.0.html) : `MAJOR.MINOR.MICRO`.  
Exemple : 0.0.1 -> initial version.

## [Unreleased]

None yet

## [0.1.0] - 2024-01-04

### 2024-01-04 : Basic features for gardening

#### Added 

Functionality:
- Pagination in /pages working (#13)
- Button aside : go back to the start of the page (#10)
- Published date & edit dates in notes (#1)
- RSS (Atom) feed (#15)
- Dates in notefeed (#14)
- Tag support (#2)

Style :
- Styling of code fence outside of pre (not handled by prism) (#5)
- Styling of tables (#6)
- Responsive iframes 8db9d10c33690d2ac22d6bcb01d072458032af40

Content :
- New notes, mostly in Livres folder 2ca9a7fdfdef29934f145845b35d9e1e2dbd8610

#### Changed

Functonality:
- Traduction of backlinks lang EN or FR (#11)

#### Fixed

- Skip to content logic (#3) (accessibility)

## [0.0.1] - 2023-08-12 : Initial release

### Initial release : basic garden

  - Basic backlinks
  - Basic notefeed
  - Anchors + hyperlinks
  - Basic blog
  - Basic accessibility with languages changes
  - Syntax highlighting with prism


<!-- CHANGELOG TEMPLATE

## [0.0.1] - 2000-01-01

### Added 

### Changed

### Deprecated

### Removed

### Fixed

### Security

Added for new features.
Changed for changes in existing functionality.
Deprecated for soon-to-be removed features.
Removed for now removed features.
Fixed for any bug fixes.
Security in case of vulnerabilities.

[…] + add reference links to the bottom of the page, in the realease order :

[unreleased]: https://[…]/0.0.1...main
[0.0.1]: https://[…]/tag/0.0.1
-->

<!-- Reference links -->

[Unreleased]: https://codeberg.org/yazae101/yazae.garden/compare/0.1.0...main
[0.1.0]: https://codeberg.org/yazae101/yazae.garden/src/tag/0.1.0
[0.0.1]: https://codeberg.org/yazae101/yazae.garden/src/tag/0.0.1