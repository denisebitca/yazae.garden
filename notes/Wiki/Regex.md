---
title: Regex
aliases:
  - Regex
notetype: unfeed
date: 2023-01-03
edit: 2023-09-03
tags:
  - Wiki
  - Code
description: Base de connaissance sur Regex.
---

## Index

[TOC]

## Tester l’expression Regex

<https://regex101.com/>

## Rechercher / remplacer avec Atom

Rechercher :  
`(1)`  
Remplacer :  
`$1`

Les parenthèses `()` permettent de capturer la valeur trouvée à l’intérieur, et les `$` permettent de la restituer.

### Convertir les dates DD-MM-YYYY en YYYY-MM-DD

Rechercher : 
```regex
^(\w+): (\d{2})-(\d{2})-(\d{4}).*
```

Remplacer :
```
$1: $4-$2-$3
```

## Variables

PCRE2 flavor (PHP 7.3+)

### Ancres

| Symboles | Description |
|:---:|:---|
| `^` ou `\A` | début d’un string  |  
| `$` ou `\Z` | fin d’un string  |  
| `\z` | fin absolue d’un string (?) |  

### Variables communes

| Symboles | Description |
|:---:|:---|
| `\` | échappe un caractère spécial |  
| `.` | tout caractère |  
| `.?` | aucun ou un caractère |  
| `.*` | zéro ou plus d’un caractère |
| `.+` | un ou plus d’un caractère |
| `.{2}` | exactement 2 caractères |
| `.{1,}` | un caractère ou plus |
| `.{2,4}` | entre 2 et 4 caractères |
| `[abc]` | un caractère a ou b ou c |
| `[^abc]` | un caractère sauf a, b ou c |
| `[a-z]` | un caractère entre a et z |
| `[a-zA-Z]` | un caractère entre a-z ou A-Z |
| `\w` | un caractère d’un mot |  
| `\W` | tout sauf un caractère d’un mot |  
| `\d` | un chiffre |  
| `\D` | tout sauf un chiffre |  
| `\s` | un caractère espace |  
| `\S` | tout sauf un caractère espace |  
