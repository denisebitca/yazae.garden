---
title: Problèmes bluetooth sur Ubuntu
aliases:
  - Problèmes bluetooth sur Ubuntu
notetype: unfeed
date: 2022-03-24
edit: 2023-09-03
tags:
  - Wiki
  - Ubuntu
description: Problème de bluetooth sur Ubuntu
---

Problème de bluetooth sur Ubuntu 20.04.4 LTS résolu par [cette marche à suivre](https://askubuntu.com/questions/1231074/ubuntu-20-04-bluetooth-not-working "Ask Ubuntu : Ubuntu 20-04 bluetooth not working") :

```
sudo apt install blueman

sudo add-apt-repository ppa:blaze/rtbth-dkms
sudo apt-get update
sudo apt-get install rtbth-dkms
```  


``sudo vim /etc/modules``

Commenter tout et ajouter cette ligne :

``rtbth``

Reboot et ouvrir :

``sudo blueman-manager``

> It worked !!

En fait, ça ouvre blueman, un utilitaire bluetooth plus performant que celui natif à Ubuntu!
