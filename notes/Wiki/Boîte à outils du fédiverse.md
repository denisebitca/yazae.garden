---
title: Boîte à outils du fédiverse
aliases: []
notetype: feed
date: 2023-07-04
edit: 2023-09-03
tags:
  - Tech
  - Fédiverse
  - Wiki
description: Ma boîte à outil pour naviguer sur le fédiverse.
---
Un répertoire de liens et ressources que j'ai rassemblé sur le fédiverse. À vocation à être enrichi au fil du temps, hésitez pas à me partager des liens cool !

## Index

[TOC]

## Définitions

Le fédiverse, késako ? C’est un réseau de plateformes de réseau sociaux qui communiquent entre eux, comme Mastodon ou Pleroma, des logiciels de microblogging (similaires à Twitter), Peertube, un logiciel de partage de vidéos (similaire à Youtube), Plume, un logiciel de blog, Lemmy, une alternative à Reddit, et pleins d’autres. Puisque ces logiciels partagent le même langage, ActivityPub, ils peuvent se fédérer entre eux.  
Une utilisatrice de Mastodon pourra mettre en favoris une vidéo postée sur Peertube et écrire un commentaire sous un billet de blog Plume sans avoir besoin d’avoir plusieurs comptes pour chaque réseau.

C’est une autre manière de penser les réseaux sociaux, puisqu’aucun des réseau n’est contrôlé par une seule entreprise, le fédiverse est constitué d’une myriade de serveurs gérés souvent bénévolement par des particuliers, financés souvent par des dons, ou par un compte payant par exemple.  
Un réseau où nos interactions ne reposeraient pas sur un algorithme voulant nous vendre des choses, mais sur de réelles connexion humaines, organisées et modérées par des collectifs indépendants : c’est ça la vision première du fédiverse.

## Outils en lignes

- **[FediDB](https://fedidb.org/)**  
  🇬🇧  
  Un site qui analyse l'activité des servers du fédiverse. Permet d'avoir un aperçu des vagues d'arrivées sur le réseau, et des tendances générales.  

- **[MastoActivity](https://mastoactivity.martlet.dev/)**  
  🇬🇧  
  Un outil pour avoir un aperçu de l'activité d'un compte. Ne semble pas fonctionner pour les boost pour le moment. Utile pour savoir si un compte a une activité anormale (bot ou autre).  
  
- **[TootExplorer](https://tootexplorer.netlify.app/)**  
  🇬🇧  
  Un outil pour avoir une vision générale des instances de nos follows/mutus, utile si on veut changer d'instances par exemple.  
  
- **[Join Mastodon](https://joinmastodon.org/fr/servers)**  
  🇬🇧  
  Un site qui permet de parcourir toutes les instances publiques de mastodon, avec un système de tri par langue, sujet, etc.  
  Ne répertorie pas les instances qui ont fermé leurs inscriptions / uniquement par invitation, donc ignore quelques instances importantes tout de même (eldritch.cafe ou weirder.earth par exemple).  

## Ressources

- **[Fedi.Tips](https://fedi.tips/)**  
  🇬🇧  
  Un site en anglais (oui, encore…) qui donne pleins d'infos et d'astuces pour des personnes non-techniques. Le site est aussi lié à un compte mastodon, [Fedi.Tips](https://mstdn.social/@feditips), qui permet de recevoir des informations ponctuelles dans sa timeline sans être submergé par les informations !  Plus généralement, le hashtag \#feditips fonctionne aussi plutôt bien pour cet usage.  
  
- **[joinfediverse.wiki](https://joinfediverse.wiki/Main_Page/fr)**  
  🇬🇧 · 🇫🇷  
  Un wiki d’introduction au fédiverse, en plusieurs langues (principalement anglais)
  
- **[wedistribute.org](https://wedistribute.org)**  
  🇬🇧  
  Un site sur l’actualité du fédiverse, maintenu par une seule personne pour le moment.
  
- **[Guide de découverte de Mastodon (et du Fédiverse)](https://mypads2.framapad.org/p/twitter-mastodon-9c2lz19ed)**  
  🇫🇷 · 🇬🇧  
  Un pad très complet qui contient énormément de ressources liées à Mastodon

- **[Mastodon for creators](https://www.eroticmythology.com/mastodon-for-creators/){lang="en"}**  
  🇬🇧  
  Un guide d’introduction en anglais au fédiverse pour les créateurices, notamment celleux qui crée des contenus "adultes".

## Astuces

### Retrouver ses tags

Auparavant, mastodon proposait d'épingler sur votre profil des hashtags, pour permettre à d'autres personnes de voir facilement ce qui vous intéresse et d'afficher les posts en question.  
Cette interface a disparu lors d'une refonte de la page de profil, mais il est toujours possible de retrouver tous vos messages sur un tag, en allant sur un lien qui ressemble à ça :  
`instance.example/@handle/tagged/tag`  
exemple réel :  
[eldritch.cafe/@Dykecyclette/tagged/artisanat](https://eldritch.cafe/@Dykecyclette/tagged/artisanat)  
On retrouve bien tous les posts de Dykecyclette qui ont le tag artisanat.

Étant donné que la recherche ne fonctionne pas très bien dans la plupart des cas, c'est pour moi un bon moyen de retrouver mes posts "importants" / sur un sujet précis, c'est toujours ça de gagné !

### Flux RSS

Si vous êtes fan de flux RSS, vous pouvez suivre le flux RSS d'un compte mastodon (tous les posts publics) très facilement :  
Il suffit d'ajouter .rss à la fin de la page de profil !  
Exemple :  
[mstdn.social/@feditips.rss](https://mstdn.social/@feditips.rss) pour le compte FediTips.

### Eldritch Radio

\#NP, \#NowPlaying, \#PouetRadio et d'autres hashtag, lorsqu'ils accompagnent un lien de musique youtube sur le fédi, servent à alimenter un outil de playlist automatique. Ça se trouve sur [radio.eldritch.cafe](https://radio.eldritch.cafe/) 😃

## Outils pour les admin & modérateurices

- **[Les blocklists Mastodon d'Oliphant.Social]( https://writer.oliphant.social/oliphant/the-oliphant-social-blocklist)**  
  🇬🇧  
  Un site très complet qui liste des blocklist universelles ou pas, pour avoir une base et bien démarrer son serveur mastodon.  
 
- **Le hashtag FediBlock**  
  🇬🇧  
  Usuellement en anglais car international, \#FediBlock permet aux administrateurices d'avertir les autres admin de problèmes liés à des instances craignos. Une bonne partie des preuves se retrouvent sur **The Bad Place**{lang="en"}, dont je parle juste au-dessous.

- **[The Bad Place](https://thebad.space/listings/page/1)**{lang="en"}  
 🇬🇧  
 Un site qui répertorie et explicite les blocklist trouvables sur Oliphant.Social et ailleurs.  

## Informations / meta fédi

### Pleroma

Pleroma est, comme Mastodon, un logiciel de microbloging faisant partie du fédiverse.  
Il a l'avantage d'être plus léger que mastodon et donc de tourner sur des plus petits serveurs. Il est aussi focus sur la paramétrabilité de l'instance, beaucoup d'options sont possibles pour donner une identité à son instance Pleroma.

Beaucoup de controverses sont liées à la gouvernance de Pleroma et son équipe de devs, tout comme les nombreuses instances d'extrême droites / confusionnistes & co qui utilisent le logiciel, ce pourquoi un hard fork de Pleroma existe, [Akkoma](https://akkoma.social/).

Une instance bordelaise, [bdx.town](https://bdx.town/), développe un front-end très joli pour Akkoma/ pleroma et mastodon, [Mangane](https://github.com/BDX-town/Mangane), et utilises Akkoma sous le capot !

### Threads & co

- **[The Two Camps of Mastodon](https://heat-shield.space/mastodon_two_camps.html){lang="en"}**  
  🇬🇧  
  Un article en anglais qui résume assez bien les discutions sur Threads, le nouveau service twitter-like de Meta (facebook), qui a pour intention de rejoindre le fédiverse *soon*{lang="en"}™️.

## Outils de devs, webmentions

- Un article (et un lien vers le code PHP) pour récupérer les commentaires des toot liés à ton blog static, et les inclure dans ton blog.  
  🇬🇧  
  [I may have found an alternative solution to my blog's comment system.](https://www.davidrevoy.com/article981/i-may-have-found-an-alternative-solution-to-my-blogs-comment-system){lang="en"}

- [D’autres idées de comment faire ça :  
  (avec Zola)](https://marisa.club/a-new-adventure/) 
  
- [Un article récent qui explique coment ajouter des webmentions sur eleventy](https://equk.co.uk/2023/07/18/adding-webmentions-in-eleventy/)

- Une liste de liens pour Webmentions + eleventy  
	https://sia.codes/posts/webmentions-eleventy-in-depth/  
	https://github.com/CodeFoodPixels/eleventy-plugin-webmentions  
	https://github.com/maxboeck/eleventy-webmentions  
	https://mxb.dev/blog/using-webmentions-on-static-sites/
