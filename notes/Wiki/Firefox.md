---
title: Setup Firefox
aliases:
  - Paramétrer Firefox
notetype: unfeed
lang: en
date: 2023-05-16
edit: 2023-09-03
tags:
  - Wiki
  - Tech
description: My setup for Firefox
---
My setup for firefox

## Plugins

### Privacy

- Clear URLs
- Privacy Badger
- Ublock Origin
- Wallabager
- Decentraleyes
- Multi-account container
- LibRedirect

### Tools

- WebToEpub
- DownThemAll !
- CopyAltText
- Save WebP as PNG or JPEG

### Others

- Snowflake
