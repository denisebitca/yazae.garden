---
title: Accessibilité Wikiversité - Session 2
aliases: []
notetype: unfeed
date: 2022-03-25
edit: 2023-09-03
tags:
  - Wiki
  - Accessibilité
description: Notes sur la deuxième session de la formation Accessibilité numérique de la Wikiversité.
credit: Tous les contenus de la wikiversité sont sous license CC-BY-SA.
---
Notes sur la [deuxième session](https://fr.wikiversity.org/wiki/Initier_une_demarche_d_accessibilite_numerique-notions_de_base#firstHeading) de la formation Accessibilité numérique de la Wikiversite.

## Index

Retour à [[Accessibilité numérique (2015) - Wikiversité|Accessibilité numérique (2015) (session 1)]].

[TOC]

## Session 2 - Initier une démarche d'accessibilité

### Introduction

L'accessibilité web impacte les 3 phases de la vie d'un site : Conception, fabrication et exploitation (y compris la publication de contenus).

### Fabrication

La qualité du dev est primordiale : plus de 50% de l'accessibilité d'un site dépendent de la phase de production du site web. Etant donné la complexité de cette phase, corriger l'accessibilité après coup sera particulièrement coûteux.  
L'intégration HTML / CSS devra être rigoureuse, valide et sémantique, tandis que le code JavaScript devra être non obstructif.

Le choix des outils, notamment du CMS ou framework, est également important : le code généré par ceux-ci doit être accessible ou facilement surchargeable.

### Exploitation

Ne pas négliger non plus l'exploitation du site par les produteurices de contenu. C'est à elleux de fournir les alternatives textuelles à chaque média (audio, visuel ou vidéo), comme les transcriptions, les sous-titres ou les img description.

### Conception

Il faut penser l'accessibilité en amont de la conception, autant dans le cahier des charges que dans les choix techniques et logiciels.

La conception graphique s’attachera à limiter les difficultés de lisibilité, de manque de contraste, etc. La « conception pour tous » implique la prise en compte, très en amont, des retours d’usage, y compris des spécificités du handicap.

### Débuter en Accessibilité

Lorsque l'on débute, mieux vaut se donner des objectifs moindre mais réalistes et atteignables. Concentrez-vous par les points bloquants mais aussi les plus simples, comme l'obligation de l'attribut "alt" sur les images.

### Former les équipes

L'accessibilité est un domaine technique qui necessite une formation.  
Faire intervenir un expert certifié ne suffit pas à garantir l'accessibilité d’un projet web. Il faut aussi former les équipes, c’est-à-dire compléter la sensibilisation globale de l’ensemble des intervenants par des formations spécifiques, selon les profils, par exemple à la conduite de projet web accessible ou à la contribution de contenus accessibles.

### Planifier l'amélioration progressive

Chaque réunion de lancement d’un nouveau projet devrait intégrer un expert, ou du moins une personne formée et rompue à l'accessibilité.

L'organisation en méthode agile est préconisée grâce à ses nombreux retours utilisateur et les corrections progressives qu'elle permet.

### Résumé

L'accessibilité est une démarche et non un état de fait. C’est un processus où l'amélioration est progressive et continue, où l’on apprend en même temps que l’on fait.  
Il faut l'adapter à la situation et à ses méthodes de travail et pas l'inverse.

Identifier les premières actions à mener et les pratiques à adopter, qui constitueront les premiers pas, permettront d'initier la démarche.


Suite : [[Accessibilité Wikiversité - Session 3]]
