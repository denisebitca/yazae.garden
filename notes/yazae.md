---
title: Yazae
aliases:
  - Yazae
notetype: unfeed
date: 2023-05-03
edit: 2023-09-03
tags:
  - Queer
description: Oui, c’est moi !
---
Oui, c’est moi !  
Je suis une meuf trans non-binaire, handie psy et pas mal geek sur les bords. Je me passionne pour les livres, l’anarchisme, les jeux-vidéos, la culture queer, les chats, le militantisme, l’archivisme, et un peu tout ce qui mixe tout ça à la fois ^^  

Dernièrement, je me renseigne sur l’[[Accessibilité numérique (2015) - Wikiversité|accessibilité]], j’écoute toujours des [[Podcasts]], je bricole ce site avec [Eleventy](https://11ty.dev/), et je débute en adminSys !

Ouais, je code, aussi, parfois (surtout du CSS), mais je suis très rouillée alors vous moquez pas !

## Me contacter

Vous pouvez me contacter sur le [[Boîte à outils du fédiverse|fédiverse]], via matrix, ou par mail : `yazae "@" murena.io` 🙂
