---
title: Gentillesse
aliases: []
notetype: feed
date: 2023-07-01
edit: 2023-09-03
tags:
  - Journal
description: "T'est gentille : rôle de genre, compliment, réaction à des traumas ?"
---
*Originellement posté en thread sur le fédiverse.*

> {{site.author}} elle me fait un A4 d'explication  
> Elle se dit que je suis trop nul là, je dois l'aider  
> Elle a dû être Saint-Bernard dans une autre vie  

On m'avait encore jamais comparée à un Saint-Bernard jusqu'à maintenant (hum peut-être que si en fait), mais il paraîtrait que je sois perçue comme "gentille" par une grande partie de mon entourage, proche ou moins proche.

Ça peut aller du "tellement gentille qu'elle a du mal à tuer des moustiques" (ce qui est faux, c'est peut-être le seul insecte que je me résous à combattre activement :p) à des extrêmes bien trop exagérés comme "{{site.author}} c'est la bienveillance incarnée"  

Je ne sais pas trop quoi penser de cette étiquette x)

D'un côté, ça valorise quelque chose d'important pour moi : mon empathie, ma recherche de compréhension de l'autre, le travail de politisation, de déconstruction que je mène sur moi-même depuis des années, et qui vise en effet à nuire au moins de monde possible et, si possible, à avoir une action positive / constructive / bénéfique sur le monde et les autres.

D'un autre côté, on remarque bien plus ce trait de caractère / on me le fait bien plus remarquer/ me complimente dessus quand on me considère comme une meuf à ce moment-là.

J'ai ce comportement, moins politique certes, mais bien ancré en moi, depuis toute petite, et les mecs cis qui me servent aujourd'hui ces compliments sur internet étaient probablement les même qui pouvaient m'exclure de leurs groupes de sociabilisation voire se moquer de moi / me harceler pour les même raisons : je ne suis pas à l'aise avec la violence, je n'aime pas me moquer des autres ou être cruelle à dessin, j'ai très peu de répondant si je suis prise à partie, je ne m'énerve quasiment jamais, je pardonne très vite, l'absence d'esprit de compétition, etc…

Il y a aussi le fait que ce compliment montre en creux des traits de mon caractère que je considère comme des faiblesses, que j'essaie de combattre :

L'évitement / la peur du conflit, le manque de hargne et de combativité, la recherche du consensus (quitte à prendre trop sur moi), et l'incapacité à se défendre activement contre des attaques physiques ou verbales.

Bref, c'est un compliment à double tranchant, et même si je suis fière de cette partie de moi, il n'a pas forcément la même saveur suivant qui me l'adresse 🤔
