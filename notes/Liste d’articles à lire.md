---
title: Liste d’articles à lire en 2023
aliases:
  - À lire
notetype: feed
date: 2023-07-09
edit: 2023-09-03
tags:
  - Militantisme
  - Liste
description: "Une liste d'article intéressants que j'ai lu / que je dois lire en ce moment."
---

Une liste d'article intéressants que j'ai lu / dois lire en ce moment.  
Principalement / quasi-uniquement des sujets politiques, car les articles tech uniquement n'apporteraient pas grand chose.

## Index

[TOC]

## Militantisme

- [« C’EST LA GUERRE ! » - Alèssi Dell'Umbria | lundi matin](https://lundi.am/C-EST-LA-GUERRE)

- [Enfin le guide pour militer de façon vraiment lutte des classes c’est à dire sans bourge dans ses rangs. | Fina](https://sylphelinetoujoursdanslaluneoudanslesnuageswordpre.wordpress.com/2020/04/22/enfin-le-guide-pour-militer-de-facon-vraiment-lutte-des-classes-cest-a-dire-sans-bourge-dans-ses-rangs/)

- [Comment faire cesser les agressions dans nos communautés – Tiens, ils ont repeint.](https://repeindre.info/2019/03/10/en-finir-aves-les-rock-stars/)  
  En finir avec les rock stars. 

- [Fomo & révolution - Pourquoi avons-nous peur de manquer le grand soir ? | lundi matin](https://lundi.am/Fomo-revolution)

- [Pas d'occup sans organisation féministe !](https://paris-luttes.info/16754)

- [Aider ses ami.e.x.s qui ont parfois envie de mourir à ne peut-être pas mourir](https://www.zinzinzine.net/aider-vos-ami.e.x.s-qui-ont-parfois-envie-de-mourir-a-ne-peut-etre-pas-mourir.html)

- [Nous sommes de toutes les luttes mais vous n’êtes d’aucunes des nôtres. | La Marginale](https://www.lamarginale.fr/2022/01/18/pride-grenobloise-2021/)

### Handicap

- [Les ESAT : générosité nationale ou entourloupe associative ? | Sandro Swordfire](http://sandroswordfire.over-blog.com/2022/04/paiement-en-esat-generosite-nationale-ou-entourloupe-associative.html)

- [Pourquoi je ne parle plus de handicap invisible \| Sandro Swordfires](http://sandroswordfire.over-blog.com/2021/12/pourquoi-je-ne-parle-plus-de-handicap-invisible.html)

- [Pourquoi je ne supporte pas l'expression "personne en situation de handicap" | Sandro Swordfire](http://sandroswordfire.over-blog.com/2022/03/pourquoi-je-ne-supporte-pas-l-expression-personne-en-situation-de-handicap.html)

- [Contre la récupération du handicap par les personnes anti écriture inclusive | REHF](https://efigies-ateliers.hypotheses.org/5274)

- [Ce que le Mouvement de la Neurodiversité permet et ce qu’il ne permet pas – Neurostyles](https://cle-autistes.fr/le-mouvement-de-la-neurodiversite/)  
  Traduction de [cet article](https://thinkingautismguide.com/2018/02/what-neurodiversity-movement-doesand.html), de Emily Paige Ballou.

### Queer

- [Terfisme et misandrie | potate.space](https://blog.potate.space/terfisme-et-misandrie/)  
  Le rapport entre terfisme et misandrie, ou plutôt, terfisme et hégémonie féminine.

- [A History of Leather at Pride: 1965-1995](https://aphyr.com/posts/358-a-history-of-leather-at-pride-1965-1995){lang="en"}

- [A Self Defense Study Guide for Trans Women and Gender Non-Conforming / Nonbinary AMAB Folks](https://www.silversprocket.net/2021/09/13/a-self-defense-study-guide-for-trans-women-and-gender-non-conforming-nonbinary-amab-folks/){lang="en"}

### Anarchisme

- [Anarchism in Practice Is Often Radically Boring Democracy | SAPIENS](https://www.sapiens.org/culture/anarchism-democracy/){lang="en"}

- [Punk—Dangerous Utopia | CrimethInc.](https://crimethinc.com/2022/12/13/punk-dangerous-utopia-revisiting-the-relationship-between-punk-and-anarchism){lang="en"}

- [Mutual Acquiescence or Mutual Aid? - Ron Sakolsky | The Anarchist Library](https://theanarchistlibrary.org/library/ron-sakolsky-mutual-acquiescence-or-mutual-aid){lang="en"}

## Tech

### Auto-défense numérique

- [Affaire du 8 décembre : le chiffrement des communications assimilé à un comportement terroriste | La Quadrature du net](https://www.laquadrature.net/2023/06/05/affaire-du-8-decembre-le-chiffrement-des-communications-assimile-a-un-comportement-terroriste/)

### Accessibilité

- [Accessibilité numérique : améliorons nos arguments ! - Access42](https://access42.net/gaad-2020-accessibilite-numerique-ameliorons-nos-arguments?lang=fr)

### FOSS

- [Funkwhale, The FOSS That Won't Flush | Ginny Mc Queen](https://ginnymcqueen.com/funkwhale-the-foss-that-wont-flush/){lang="en"}

## Backlog

Les articles que j'avais mis dans cette liste par le passé

### 2022

- [Usbek & Rica - Soan, papa transgenre, « transparent mais pas invisible »](https://usbeketrica.com/fr/article/soan-le-parcours-d-un-papa-transgenre-face-a-l-invisbilisation-de-la-transparentalite)
