---
title: Contribuer sur Wikipedia
aliases:
  - Getting started on Wikipedia
notetype: unfeed
date: 2022-12-24
edit: 2023-09-03
tags:
  - Libre
  - Draft
description: Toutes les infos pour contribuer à Wikipedia
---
Je réunirais toutes les infos que je rassemblerais sur Wikipedia et la manière dont il est possible de contribuer à cette encyclopédie participative.

## Index

[TOC]

## To do

- ~~Créer page de la créatrice de Pikachu~~
  - Corriger sa page en anglais
- ~~Lister pages à étudier / surveiller / enrichir~~
- Se renseiger sur règles de wikification

## Règles de Wikipedia

- WP est une encyclopédie
- Rédigée sur un ton neutre
- Publiée sous licence libre
- Régie par règles de savoir vivre
- WP n'a pas d'autres règles fixes
  - Les autres règles sont définies par délibération et concensus, au niveau global ou individuel de chaque pages

### Wikification

- Utiliser le nom de famille des personnes et pas leur prénom
- Nom d'une bio / sujet d'un article en **gras** au début de l'article (RI = Résumé introductif)
- Écrire la suite !

### Enrichir une page

Une information = 1 source vérifiable

Source secondaire > Source primaire

__Critères de notoriété :__
- Deux sources différentes
- Sources centrées
- Secondaires (écris par la personne ou dit par elle ne compte pas)
- Fiable et de qualité
- Espacées d'au moins deux ans

Portrait dans Le Monde / libération / Le Figaro / Le Parisien.

## Les Sans pagEs

### Les biais de genre dans WP

2013 - 16% contributrices  
2022 - 19% de biographies de femmes (15% en 2015)

Différence dans les domaines de connaissances (exemple : arts textiles très sous-dev)

Sous-représentation de :
- Littérature
- Sciences humaines
- Mode
- Artistes
- Sportives

Problème dans le langage des articles

- c'est la femme de ..
- Vie privée enfants mariages 4%H et 27% F
- 1ères femmes à faire quelque chose : expliquer ce qu'elle a fait

- On ne passe pas dérrière qq1 pour changer le nom de fonction  
Droit au langage inclusif mais pas de point médian :
- "personnalité du monde du cinéma"
- "double flexion"

## Mon activité sur Wikipedia

- [Ma page personnelle](https://fr.wikipedia.org/wiki/Utilisatrice:Yazaæ)
- Dernières pages crées :
  - [Atsuko Nishida](https://fr.wikipedia.org/wiki/Utilisatrice:Alex_Lambda/Atsuko_Nishida), créatrice de Pikachu.
  - [Emily Skidmore](https://fr.wikipedia.org/wiki/Emily_Skidmore "Emily Skidmore"), autrice de True Sex.

## Pages à surveiller

[Erik Schinegger](https://fr.m.wikipedia.org/wiki/Erik_Schinegger)
