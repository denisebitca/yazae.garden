---
aliases: []
title: PAL
notetype: feed
date: 2022-07-24
edit: 2023-12-28
tags:
  - Liste
  - Livres
  - Queer
description: Vous trouverez ici ma PAL(pile à lire) non-exhaustive, principalement remplie de bouquins queer 📚
---

Vous trouverez ici ma PAL(pile à lire) non-exhaustive, principalement remplie de bouquins queer 📚  

J’essaierais d’écrire une critique dans [[Les micro-critiques de Yazae]] quand j’aurais terminé l’un deux !

## Index

[TOC]

## Fantasy

Pourquoi lire de la fantasy quand on peut lire de la fantasy queer ? :3

### Fur and Fangs
**Fur and Fangs**, de Rae D. Magdon. Langue : anglais.  
CW/thèmes : Urban Fantasy, Romance.

<img src="../../assets/img/notes/PAL/FurAndFangs_cover.jpg" alt="Fur and Fangs cover" width="50%"/>

### This Poison Heart
**This Poison Heart**, de Kalynn Bayron. Langue : anglais.  
CW/thèmes : Urban Fantasy.

<img src="../../assets/img/notes/PAL/poison-heart_cover.jpg" alt="This Poison Heart cover" width="50%"/>



### Legendborn

**Legendborn**, de Tracy Deonn. Langue : anglais.  
CW/thèmes : Urban Fantasy.

<img src="../../assets/img/notes/PAL/legendborn_cover.jpg" alt="Legendborn cover" width="50%"/>

> The main characters in Legendborn aren’t LGBT (well, there’s a thing but I’m not going to say anything because spoilers, darling) but a good many of the characters surrounding them are. And while there have been reams of Round Table slash fic written through the ages, what I appreciated most about the way Deonn included LGBT content is that she simply…included it. The Scions in Legendborn may be interacting with extremely conservative and traditional personalities from long ago, and while those personalities certainly have some traction in terms of control, neither the Scions nor the other members of their courts are about to get steamrolled by outdated mores and cultural hangups where sexuality and gender identity are concerned. Alas, some of them aren’t as progressive about race, but that’s a different post.

### White Trash Warlock

**White Trash Warlock** de David R. Slayton. Langue : anglais.  
CW/thèmes : Urban Fantasy.

<img src="../../assets/img/notes/PAL/white-trash-warlock_cover.jpg" alt="White Trash Warlock cover" width="50%"/>

> Adam Binder’s family would rather have him involuntarily committed to a mental health facility than believe in his magical abilities. They aren’t all that much more accepting about the fact Adam is gay. Adam is pretty sure that’s just the way it is.
>
> He’s proven wrong, however, when he meets Vic, a police officer who becomes accidentally embroiled in Adam’s efforts to thwart the eldritch horror hovering over Denver. Vic hasn’t considered the fact he might be bi, before but finds himself attracted to Adam and without any drama whatsoever, thinks: ah. Okay. Bi. Cool. His family meets Adam and thinks, What a sweetheart. Glad Vic found him. Sum total. That’s not to say the two men figuring one another out and navigating a potential relationship is simple or without nuance; it means they can continue their efforts to prevent the apocalypse while they do it.

### Cemetery Boys

**Cemetery Boys** de Aiden Thomas. Langue : anglais.  
CW/thèmes : Urban Fantasy.

<img src="../../assets/img/notes/PAL/cemetery-boys_cover.jpg" alt="Cemetery Boys cover" width="50%"/>

> One of the things I loved most about this LGBT urban fantasy is the fact that while Yadriel’s family may have difficulties accepting the fact he’s trans, his deity never does (not a spoiler, it’s in the first chapter). While that might not be enough for Yadriel, it does help him believe he deserves his place in the world and grant him the confidence he needs for his journey.
>
> I also adore Julian’s deep acceptance of the fact he’s gay; it’s not something we see in bad boy, macho characters on the regular and it’s so damn special, both as a reader who didn’t insist on her parents working to understand “bi” and later “pan” because she was just too tired of being judged, and as a parent who wants her kids to know they can be anything and anyone they damn want.

### Testament T01 - L'Héritière  

**Testament T01 - L'Héritière**, de Jeanne-A. Debats. Langue : français.  
CW/thèmes :  Urban Fantasy

<img src="../../assets/img/notes/PAL/testament-T1_cover.jpg" alt="Couverture de Testament T01 - l'Héritière" width="50%"/>


> Je m'appelle Agnès Cleyre et je suis orpheline. De ma mère sorcière, j'ai hérité du don de voir les fantômes. Plutôt une malédiction qui m'a obligée à vivre recluse, à l'abri de la violence des sentiments des morts. Mais depuis le jour où mon oncle notaire m'a prise sous son aile, ma vie a changé. Contrairement aux apparences, le quotidien de l'étude qu'il dirige n'est pas de tout repos : vampires, loups-garous, sirènes… A croire que tout l'Alter Monde a une succession à gérer ! Moi qui voulais de l'action, je ne suis pas déçue… Et le beau Navarre n'y est peut-être pas étranger.

## SF  

### Spellhacker  

**Spellhacker**, de MK England. Langue : anglais.  
CW/thèmes : nb mc, mc with chronic illness, queer mc  

<img src="../../assets/img/notes/PAL/spellhacker.jpg" alt="Spellhacker cover" width="50%"/>

  >  set in a world with tech and magic, really really cool, one of the main character is non binary and has a chronic illness <3 There are a few annoying thing in this book plot wise as the author is at the very beginning of her career I think and I wish it would have gone through a few more rounds of editing but overall I really loved it!!! (Loke)

  > Can only agree with Loke! The universe is really really cool, it’s sort of a dystopia with magic that can be used either organically or via technology, and it’s fascinating! I’ve got a big beef with the mc who’s always angry at her friends with no reason, but the overal book felt like a good heist movie. (Azaliz)
