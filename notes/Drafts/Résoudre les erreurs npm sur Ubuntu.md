---
title: Résoudre les problèmes de npm sur Ubuntu
aliases:
  - Résoudre les problèmes de npm
notetype: unfeed
date: 2023-08-25
edit: 2023-09-03
tags:
  - Wiki
  - Tech
  - Draft
  - Ubuntu
description: Résoudre les problèmes de npm sur Ubuntu 12
---

En installant ubuntu 12 LTS (jammy) et en essayant de rebuild mon site sur [[Eleventy - 11ty]] comme je le fais d'habitude, je me suis retrouvée confrontée à plusieurs erreurs : 

## installer nvm

- la version de nodejs utilisée dans les dépôts d'ubuntu était bien plus ancienne que la version 18 utilisée dans mes dépendances  
Pour résoudre ce problème, on conseille généralement de passer par nvm, *Node Version Manager*{lang="en"}.

On le trouve [sur son dépôt officiel](https://github.com/nvm-sh/nvm#installing-and-updating).  
On fait confiance à son script où [on regarde ce qu'il fait ](https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.5/install.sh): 

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.5/install.sh | bash
```

Une fois nvm installé, on relance le terminal et on lui demande d'installer le dernier node en date

`nvm install node`

On a plus qu'a utiliser node sur nos projets !

## npm : permission refusée

- Même après avoir résolu ce problème, j'avais une erreur bizarre : 
```bash
sh: 1: npm-run-all : permission denied
```

Les différentes solutions proposées sur internet m'ont amenée sur des contrées beaucoup trop compliquées et sauvages, quand enfin au détour d'un stack overflow, une solution toute simple : "j[ust delete the module folder and reinstall it](https://stackoverflow.com/questions/62697156/sh-1-node-gyp-build-permission-denied-and-npm-err-code-elifecycle-npm-err-e#comment110875312_62697156)"

Certains fichiers devaient avoir des autorisation d'anciens users pas sur cette machine.  
Supprimer le dossier node-module et refaire un `npm install` dans le dossier a tout résolu ! 😁
