---
title: Écriture inclusive et neutre
aliases:
  - Écriture inclusive et neutre
notetype: unfeed
date: 2017-07-11
edit: 2023-09-03
tags:
  - Féminisme
  - Militantisme
  - Draft
description: Différentes sources qui donnent des pistes de réflexion sur l’écriture inclusive.
---
Différentes sources qui donnent des pistes de réflexion sur l’écriture inclusive.

## To Do

- Mettre au propre ces notes

## Ressources

- [CodePen - Féminiser les textes](https://codepen.io/vincent-valentin/full/woGLVL/)  
  Liste des abréviations inclusives et leurs avantages / inconvénients

- [À celleux qui me lisent - romy.tetue.net](http://romy.tetue.net/a-celleux-qui-me-lisent)  
  Sur l’usage de celleux
  
- [Inclusivité & accessibilité - Lunatopia](http://lunatopia.fr/blog/inclusivite-accessibilite)  
  Ça donne une réponse sur comment est lu le texte avec les point médians : avec les lettres prononcées distinctement apparemment, mais ça diffère selon les lecteurs d’écrans.

- [Écriture inclusive au point médian et accessibilité : avançons vers des solutions](https://www.lalutineduweb.fr/ecriture-inclusive-accessibilite-solutions/)

- [Contre la récupération du handicap par les personnes anti écriture inclusive | REHF](https://efigies-ateliers.hypotheses.org/5274)

- [Alpheratz - Un genre neutre pour la langue française](https://www.alpheratz.fr/articles/un-genre-neutre-pour-la-langue-francaise/)

- [Désexualiser l’écriture : romans et traduction épicènes](https://www.en-attendant-nadeau.fr/2017/08/01/roman-traduction-genre/)

En vrac (pas toutes les sources se valent, mais c'est intéressant) :

- [cafaitgenre.org/2013/12/10/feminisation-de-la-langue-quelques-reflexions-theoriques-et-pratiques/comment-page-1/](https://cafaitgenre.org/2013/12/10/feminisation-de-la-langue-quelques-reflexions-theoriques-et-pratiques/comment-page-1/)

- [madmoizelle.com/guide-langage-non-sexiste-109220](http://www.madmoizelle.com/guide-langage-non-sexiste-109220)

- [romy.tetue.net/feminiser-au-point-median](http://romy.tetue.net/feminiser-au-point-median)

- [fr.wikipedia.org/wiki/Langage_épicène](https://fr.wikipedia.org/wiki/Langage_%C3%A9pic%C3%A8ne)

- [biscuitsdefortune.wordpress.com/2015/06/20/loqlf-et-la-feminisation-survol-et-critiques/](https://biscuitsdefortune.wordpress.com/2015/06/20/loqlf-et-la-feminisation-survol-et-critiques/)

- Un scan de l'article de Well Well Well # 2 sur l'écriture inclusive (PDF): [WellWellWell_écriture-inclusive.pdf](../../assets/docs/notes/Inclusive/WellWellWell_écriture-inclusive.pdf)
