---
title: Les micro-critiques de Yazae
aliases:
notetype: feed
date: 2024-01-04
edit: 2024-01-04
tags:
  - Livres
  - Fantasy
  - Queer
description: Vous trouverez ici toutes mes critiques/review écrites en français sur Mastodon qui parlent de livres 📚
---

Vous trouverez ici toutes mes critiques/review écrites sur Mastodon qui parlent de livres 📚  

Les plus longues auront un article dédié, mais je préfère réunir tout dans une page plutôt que me disperser.

Vous pouvez aussi lire [[The micro-reviews of Yazae |mes micro-critiques en anglais]].

## Index

[TOC]

### Non-Player Character

**Non-Player Character**, de [Veo Corva]().  langue : anglais.  
**CW / thèmes :** Fantasy, LitRPG, Asexualité, Cosy Fiction.  

<img src="/assets/img/notes/critiques/NonPlayerCharacter_cover.jpg" alt="Image de couverture de Non-Player Character. Dans une forêt aux tons bleus et violets, un personnage aux cheveux blancs s'approche l'air curieux d'une créature qui flotte près d'iel" width="50%"/>

Aujourd'hui, après une énième relecture, je voulais vous parler de **Non-Player Character**.

C'est une œuvre de Portal fantasy / LitRPG, qui rentre dans le champ de ce qu'on pourrait appeller 'cosy fiction', des œuvres résolument optimistes, bienveillantes avec leurs personnages et leur lectorat. Comme Corva le dit iel-même :

> Je pense que si tu tombe amoureux des personnages et de l'univers, alors toute menace envers elleux, quelle que soit sa taille, va paraître monumentale 

Et c'est bien ce qui se produit à la lecture de l'ouvrage : on a beaucoup d'affection pour Tar et sa famille choisie /ses camarades de jeux de rôle, on empathise à mort avec ses crises d'angoisses et sa gêne perpétuelle vis à vis de l'espèce humaine, et les enjeux n'ont pas besoin d'être le sauvetage du monde ou la tuerie de millier d'ennemis intuables pour que leur périple dans un monde de fantasy soit merveilleux, fascinant et pleins de moments forts.

Justement, ça fait du bien d'avoir des œuvres où la résolution des conflits ne se fait pas systématiquement par la violence, avec des personnages handis et neuroatypique dont la simple survie quotidienne est un enjeux.. Ça ressemble plus à ce qu'on vivrais moi et mes potes en cas de teleportation malencontreuse dans un monde de fantasy, avec toutes les galères que ça implique, et c'est juste génial à lire 💜

### 2 novellas de SFF par Aeph (18/08/23)

**La Sorcière, La Hyène et le garde manger**, ansi que **Green Lady** par Aeph.  langue : français.  
**CW / thèmes :** Fantasy, lesbiannisme, parodie / Science-fiction, lesbiannisme, IA.  

<img src="/assets/img/notes/critiques/greenLadySorciereHyene_covers.jpg" alt="deux couvertures des livres présentés, La Hyène, la Sorcière et le Garde-manger nous présente une hyène violette en fond avec une meuf blanche lesbienne aux cheveux roses et side-cut au premier plan.  
Green Lady nous présente un couple de femmes qui s’enlacent dans un fond de ville futuriste. L’une des femmes est une femme noire aux longs cheveux à l’air un peu triste ou fatiguée, l’autre a des cheveux aux reflets verts, les yeux verts et lèvres vertes. On comprends qu’elle est un robot, sa main semble articulée comme une marionette." width="70%"/>

Pour ce Vendredi Lecture, je vous présente [2 novellas par Aeph](https://ybyeditions.fr/publisher/aeph/), assez classiques dans leur genre mais très cool à lire.

Les objets en soit sont très bien faits, Yby éditions connais bien son taff, et les couv’ en jettent.

La Hyène, la Sorcière et le Garde-manger est un conte parodique reprenant le mythe de la méchante sorcière, avec pour guise de blanche-neige une princesse lesbienne punk.

Green Lady, de son côté, reprends le mythe de l’amour avec une IA, (ça fait très penser au film Her), mais avec un twist et le fait qu’encore une fois, c’est une histoire d’amour lesbienne.

Pour être tout à fait honnête, j’ai beaucoup aimé Green Lady alors que La Hyène m’a moins parlé, malgré un essai de critique de classe (entre la princesse et son peuple vassal), ça restait une histoire très loufoque et "enfantine".  
J’ai peut-être été moins sensible au ton parodique d’une histoire qui restait adolescente dans ses thèmes, je ne saurais pas trop à quel public le conseiller.

Green Lady m’a frappée en plein cœur parce que j’ai un cœur d’artichaut bien entendu, mais pas que, en moins d’une centaine de pages Aeph nous dessine un monde certes classique mais tangible, et une histoire d’amour qui l’est tout autant. Une belle découverte qui donne envie d’en lire plusse :purple_sparkling_heart:​

Si vous cherchez des petits cadeaux à faire ce genre de format est parfait pour ça, franchement je conseille à tout le monde de parcourir le site de [Yby](https://ybyeditions.fr/catalogue/), beaucoup de choses donnent envie dans leur catalogue ✨  
(faut encore que je lise le recueil Histoires de Fleurs :3)

(non, ceci n’est pas un blog sponsorisé du tout, mais c’est difficile de résister à faire de la pub quand on tombe sur un boulot aussi cool !)

### La Sorcière captive - Les Faucons de Raverra T01 (19/08/22)

**La Sorcière captive**, de Melissa Caruso.  VO : anglais, trad FR.  
**CW / thèmes :** Fantasy, Politique, Sorcières, Magie, Bisexualité.  

<img src="/assets/img/notes/critiques/lesFauconsDeRaverraT1_cover.jpg" alt="Couverture de Les Faucons de Raverra tome 1" width="50%"/>

J'ai lu La Sorcière captive de Melissa Caruso, et c'est une bonne découverte :3

On y suis deux meuf badass à leur manière, très différentes l'une de l'autre, une riche héritière passionnée par la magie et contrainte à faire de la politique, et une sorcière extrêmement puissante & voleuse à la tire à ses heures perdues. Elles sont forcées à collaborer et ne s'entendent pas très bien, mais vont apprendre à respecter l'autre, au fur et à mesure des épreuves qu'elles traverses et des intrigues dans lesquelles elles sont plongées.

L'histoire repose bien plus sur des intrigues politiques que des aventures épiques, mais c'est bien raconté et on crois à l'univers dessiné et aux motivations de chacun.e. Côté romance, la principale est hétero, mais notre sorcière est bi et cela ne pose aucun problème, il y a plusieurs personnages LGBT dans l'histoire et ils sont acceptés et intégrés dans la société comme tout le monde, ça change !

Bref, de la représentation, une histoire qui tiens la route et des personnages attachants, j'ai envie de lire la suite :)

(à savoir, c'est une trilogie mais on peut tout à fait se contenter du premier tome, on ne finis pas en cliffhanger ni rien ^^)

### Mooncakes (24/07/22)

**Mooncakes**, de Suzanne Walker & Wendy Xu.  Langue : anglais.  
**CW / thèmes :** Comics, Urban Fantasy, Sorcières, Loup-garous, Romance Queer, Rupture familiale.

<img src="/assets/img/notes/critiques/mooncakes_cover.jpg" alt="La couverture du comics Mooncakes, avec une jeune sorcière qui à l’air d’avoir préparé des gâteaux, et une personne louve-garou qui est en train de lécher le plat ayant servi à faire les gâteaux." width="50%"/>

J’ai lu Mooncakes aujourd’hui, et je suis très contente de cette découverte !

C’est super mignon même si un peu naïf, l’histoire d’une ado sorcière qui aide ses grand-mères dans leur boutique de sorcellerie, et qui va aider Tam, un·e loup-garou ami·e d’enfance (qui utilise le pronom They) et potentiel crush à combattre un démon de la forêt.

Iels sont trop mignon·nes ensembles, Tam est super cool, et Nova aussi, elle porte des appareils auditifs et c’est bien utilisé dans l’histoire, les grand-mères sorcières lesbiennes sont trop cools, bref, y’a de la representation, c’est super mignon, y’a de la magie et des démons, que demander de plus ? 🥰

En plus le dessin est très réussis, des couleurs pastels qui marchent bien avec l’histoire et l’ambiance du récit, bref, je conseille cette lecture très fortement :3

### <span lang="en">The Coldest Touch</span> (09/04/22)

**The Coldest Touch**{lang="en"}, de Isabel Sterling. Langue : anglais.  
**CW / thèmes :** Meurtres, Romance lesbienne, Vampires, Bit-lit, YA.

<img src="/assets/img/notes/critiques/theColdestTouch_cover.jpg" alt="Couverture de The Coldest Touch" width="50%"/>

J'ai terminé **The Coldest Touch**{lang="en"}, et j'avoue que je suis déçue, un manque de cohérence du début à la fin, des enjeux beaucoup trop importants pour des ados immatures as fuck (surtout Claire, insupportable), des fils d'intrigue non résolus comme si on les avait oubliés, bref c'était assez décevant, j'ai lu des fanfics/bouquins amateurs mieux écris.

On parle d'un 'twilight lesbien' sur Goodread mais la vérité c'est que Twilight est mieux écris en comparaison !🤷‍♀️

J'ai pas réussi à croire à l'univers posé, et ça m'arrive rarement dans mes lectures… Pourtant y'a un personnage trop cool qui utilise le pronom They, c'est bien une romance lesbienne avec des vampire, mais voilà, je n’arrive pas à le conseiller ^^'

### <span lang="en">The Queen and the Woodborn</span> (25/06/21)

**The Queen and the Woodborn**{lang="en"}, de Shiniez. Langue : anglais.  
**CW / thèmes :** Webtoon, Fantasy médiévale, Romance lesbienne.

<img src="/assets/img/notes/critiques/theQueenAndTheWoodborn_cover.jpg" alt="Couverture de The Queen and the Woodborn" width="40%"/>

Bonjour, je ne vous ai pas encore parlé de <span lang="en">The Queen and the Woodborn</span>, une des nouvelles œuvre de Shiniez (Sunstone)?

On est dans une fantasy médiévale assez classique : c'est l'histoire d'une reine et d'une mère, qui, pour sauver son fils va s'aventurer dans une forêt mythique et interdite, quitte à mettre en danger son âme. Elle y rencontre une (très belle) sorcière, qui va lui sauver la mise, mais qui attends peut-être quelque chose d'elle en retour…

Le début d'une romance tragique ? quelque chose de plus léger ? de plus épique ? Aucune idée ! L'histoire ne fait pour le moment que 5 chapitres et l'auteur prends beaucoup de temps sur le rythme de sorties.

Mais, comme d'hab, de très beaux dessins, des personnage bien écris, une narration un poil trop verbeuse, en tout cas les ingrédient sont là et la potion semble fonctionner :)

Tout comme Sunstone, on semble se diriger vers une romance lesbienne,  écrite donc par un mec cis, mais si c'est du niveau de cette dernière, je signe !!

Pour le moment, ça n'existe qu'en anglais, sur Webtoon, mais ça vaut déjà le coup d’œil (ce dernier chapitre 😍)

[Vous pouvez le lire sur Webtoon ici.](https://www.webtoons.com/en/challenge/the-queen-and-the-woodborn/list?title_no=502306)

### Kushiel (06/06/21)

**Kushiel**, de Jacqueline Carey. Langue : anglais mais une très bonne traduction française.  
**Thèmes :** Fantasy, BDSM, TDS, Bisexusalité, Polyamour.  
**CW :** Viol, torture, traite d'être humains, même si rien n'est gratuit.

<img src="/assets/img/notes/critiques/kushiel_cover.jpg" alt="Couverture de Kushiel tome 1 - La Marque" width="50%"/>

Voir ma [[Critique de Kushiel]].

### Mes ruptures avec Laura Dean (24/08/20)

**Mes ruptures avec Laura Dean**, de Mariko Tamaki et Rosemary Valero-O'Connell  
**CW / thèmes :** Comics, Relation toxique, Relation lesbienne, Personnages queers  

<img src="/assets/img/notes/critiques/lauraDean_cover.jpg" alt="Couverture de Mes ruptures avec Laura Dean" width="50%"/>

L'héroïne est sous l'emprise de la belle et populaire Laura Dean, qui pourtant la traite comme un mouchoir usagé et la jette à la moindre occasion.

On la suis donc dans son cheminement et ses difficultés à sortir de cette relation qui l'étouffe et dégrade ses rapports avec ses proches.

La bichromie utilisée donne un effet presque éthéré, réflexif au dessin de Valero-O'Connell, qui sers bien le récit.

Outre le sujet qui me semble intéressant à traiter, la force du titre repose aussi sur la belle galerie de personnages qui entourent l'héroïne et la soutiennent malgré ses difficultés à sortir de cette relation :)

Intéressant et actuel !
