---
title: Yazae's erotic microblogging
aliases:
  - Yazae's erotic microblogging (EN)
notetype: feed
lang: en
date: 2022-12-22
edit: 2023-09-11
tags:
  - Livres
  - BDSM
  - Sexe
description: You will find here all my reviews written in english about erotic stories.
---
You will find here all my reviews written on Mastodon about erotic stories. 📚🥵📚 <br/>
CWs will be at the start of each posts.

The reviews in french will be found on [[Les critiques érotiques de Yazae]].

## Index

[TOC]

### Penny and Megan (02/12/21)

**Penny and Megan**, by VirginiaMarieAndrews. Lang : EN.  

**CW / themes :** (light) Dubious consent, Mistress/pet, F/f, Pet play, Soft, ~24/7, Topping from the bottom  

The author is the same author who have written the very good **Erin's Morning**, you can see my [review in french](https://yazae.me/notes/Les-critiques-%C3%A9rotiques-de-Yazae-(FR)#erins-morning-071120) about it.

**Story**:  
Megan is a lawyer who happens to be called in a case about CNC slavery. Penny is about to be (+/- consensually) selled to another owner, but she isn't happy with it (yet, as a dutiful slave, she obey).

Megan, afraid for Penny, outbid the eventual owner, and become the very lost and unwilling owner of a beautifully cute human kitten.

We follow their story, Megan learning how to be a good Mistress for her pet, Penny learning to regain agency and fulfilment in human life too, but mostly content to have found a kind owner who could love her.

**Why do I like it**:  
The best part of the story for me is clearly Penny. She knows what she wants, she is a slave and a kitten but she can stand for herself… at least to tease Megan and teach her how to be her Mistress ^^

It's a 'topping from the bottom' dynamic in most of the story, and I feel it works very well, for once it's the dom who is shy, insecure and don't know anything about BDSM. The twist is funny and works well :)

**Writing flaws**:  
This story have inconsistencies and weird writing choices from time to time, mainly:

-   A bit of a Mary Sue feeling about Megan, too much self-insert, dunno
-   A boyfriend with huge potential drama set in place but never used (for the better u_u')
-   The introduction of 'powers' who aren't really used except for things who could have been managed easily without them
	-   Attention : a telepathic power. It's mainly used to lessen the 'dubious consent' part of the story, since Megan _knows_ the emotions of Penny and know if she is consenting to smth, it's a writing shortcut
	-   Influence : never really used in the story, Megan could compel her pet to do… anything, but don't use it, so it have strictly no use.
-   And, the ending is a bit poor for my taste, too much use of suspension of disbelief I feel. But at least it have an end :D

**What I dislike**:  
Mostly, the 'everybody is rich' setting. Yet another BDSM story were I can't find myself at all in the character's life :/

[https://www.literotica.com/s/penny-and-megan-pt-01](https://www.literotica.com/s/penny-and-megan-pt-01 "https://www.literotica.com/s/penny-and-megan-pt-01")

### The Stanford Higschool Project (30/10/21)

**The Stanford Higschool Project**, anonymous author. Lang : EN.  

**CW / Themes :** Dubious consent, bullying, Mistresses/slave, BDSM, FFF-f  

So, this story is a draft at most, is unfinished and was originally posted on pastebin, I have it only because I saved it when reading. I don't know who the author is, and if they will post their story elsewhere at some time.

Synopsis :  
Kiara and her friends have a film-project to do, about slavery. She end up being the slave, is teased by her friends, and it somehow continues after the project, her 3 friends exploring their power upon her.  
~

To be honest, it seems like one of the first writing of the author, and it shows. The writing is pretty naive, it lack descriptions and realism, we don't always understand the reactions of the protagonists, and it's very short, left uncompleted when the real power dynamic is installed.  
It shows ignorence of the BDSM scene too.

But, still, there is some themes echoing in me :  
Kiara is pretty shy, and go along with her humiliation without resisting. There is a thrill about it, how she submit almost naturally, how the control of her friends increase along with her submissiveness.  
She could free herself of the power dynamic just by telling her friends to stop… but she don't. She allows her friends to progressively give her order, to punish her, to tease her by orgasm denial, just because she don't resist and accept her place, somehow.

I think it's the combo of compliance and shyness, I don't know, even if the story isn't really well written and pretty soft, it ignite something in me ^^

[https://framadrive.org/s/jQX6ekrLDSJtgGD](https://framadrive.org/s/jQX6ekrLDSJtgGD)

### The Long Road Home (26/10/21)

**The Long Road Home** by LunaCeMore. Lang : EN.  

**CW / themes :** Dubious consent, BDSM, SM, ff, Mistress/pet, Once Upon a Time, Evil Queen(Regina Mills) / Emma Swan

So, this story is a classic I believe, at least it is for me. I's the best smut fanfic I know of Once Upon a Time, and it's also a good BDSM work.

Emma get back in time to the Enchanted Forest, and get captured by the Evil Queen, who took her as a prisoner, a pet, and, maybe, a lover.

Emma isn't really given a choice, but she is a masochist, she already have feels for Regina, and she embrace this new life almost entusiastically.

We see them growing closer and closer in the story, overcoming past traumas and trust issues, and their relashionship is pretty cute in the end <3

Only flaw : way too quick, would have loved a lot more of smutty chapters, but it's at the cost of characters development and a good consensual ending, so I'm not that sad about it.  
It is just making me re-read this story often x)

What do I love in this story ?  
Well, the Mistress / pet dynamic is a good one, even if Emma and Regina are both bigger than life in their roles x)

Regina found for the first time a pet who is willing to suffer for her, who like it, she loves how it feels to have a true submissive in her hands, and it's adorable :3

Emma is a bit too perfect as a slave/pet, she never discuss anything, she submit beautifully, and yet she manage to stand out for the good of her Mistress, never shy, always loving and proud…

She is a bit annoying, but her entousiasm makes the whole dubcon thing almost irrelevant, and allows to enjoy the romance more, I feel. I would have liked a less perfect version of herself, but the story is already really good as it is :)

[https://archiveofourown.org/works/9213338](https://archiveofourown.org/works/9213338 "https://archiveofourown.org/works/9213338")
