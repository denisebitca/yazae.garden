---
title: The micro-reviews of Yazae
aliases: 
notetype: feed
lang: en
date: 2023-08-18
edit: 2023-09-12
tags:
  - Livres
  - Queer
description: You’ll find here all my reviews written in english on the fediverse who are about books 📚
---
You’ll find here all my reviews written on the fediverse who are about books and written works of art in general 📚  

There is a french version of this with other book review written in french, [[Les micro-critiques de Yazae|you can find it here]].

## Index

[TOC]

### Wildflower (20/08/23)

**Wildflower**, by Cathleen Collins  
**CW / themes :** Survival, Lesbian romance, Mental health

<img src="/assets/img/notes/reviews/wildflower_cover.jpg" alt="It’s the cover of Wildflower, with a forest if the background and a girl hugging a dog at the front.  
It’s a bit of a cheap cover for a book." width="50%"/>

So it’s a preview of a title given to me when asking for it at netgalley, to be transparent. It will not change what I’ll say about it :)

As I see it, it’s a Robinson Crusoe kind of book, at least in the first part : the survival of a girl (a woman after a while) almost alone in the forest after a plane crash.  
Then, the inevitable clash when comes the civilisation, and how Lily will adapt to it.

And of course, the romance, with a fated lover 💜​

I read the book almost in one go, and it was entertaining, but it was lacking in realism in several parts I can’t pin down now.  
There is also a huge shift near the end of the story who feels a bit surreal, made up, to let the story advance.

Nevertheless, the romance was good, even if it’s a *very* slow burn, and even if I wanted them to kiss 100 pages before, the ending is satisfying.

A lesbian Crusoe is a good idea, and I enjoyed it, but to be honest, I can’t really recommand it, it didn’t click with me and even if I’m always up to cheesy lesbian romance, there were too much part feeling surreal that I wasn’t really anchored in the story.

Other things who bothered me :  
- Being rich solve everything, yay  
- The fated lover trope

But I did really enjoyed it, it was a good distraction ^^

### Malice T01 (08/02/23)

**Malice**, vol 1 of the Malice duology, by Heather Walter.  
**CW / themes :** Fantasy, Yong Adult, LGBT, Lesbian romance, Fairy Tales

<img src="/assets/img/notes/reviews/malice_cover.jpg" alt="Cover of Malice. There is two feminine hands bound by a rose with lots of needle next to the title." width="50%"/>

So, it’s a complete rewrite of Sleeping Beauty, we aren’t really in a fairy tales setting but in a brand new fantasy world, with Humans, a Fae species and "dark" fae one called Vila, who have been wiped out in the last war.

Our main character, Alyce, is a half-vila, and is despised by all in the city. Contrary to the other "half-breeds" called the Graces, her powers are nefarious and wicked, she can’t heal or make someone beautiful, but she can kill, poison or grow warts on your face.

She lives in the human society, where fairy magic is hugely controlled by the power, and where the magic users are kept in golden cages, in service to the nobility & rich people, and without the right to leave the country or refuse any service, at the cost of their health.

There is also a curse in this story, of course : centuries ago, one of the last Vila cursed the crown by vengeance, such as all the descendants of the Queen are dying at their 21th birthday if they didn’t find TrueLove!™️ yet.

Alyce will realize slowly that her heritage is deeper than she thought, she will find a friend in the cursed princess, or perhaps even more, and they could have found a happily ever after… unless the world around them is structured to take advantage of their powers and strip them of their agency, unless politics and bigotry can get in the way of love, unless Alyce finally becomes the villain everyone expects her to be. Who knows ?

~

It was a really exciting read, and a heck of a ride, I’m not fully recovered yet ! The finale end in a hell of a cliffhanger, and I can’t wait to read the 2nd volume to know how all of this will be concluded :3

I really hope that the characters will find a kind of happy ending, but it will be really hard to get after the ending of this volume, we shall see o/

### Blood Witch - Blood Canticles T01 (03/02/23)

**Blood Canticles T01 - Blood Witch**, by Naomi Clark.  
**CW / themes :** Urban Fantasy, Lesbian romance, Witchcraft, Demons

<img src="/assets/img/notes/reviews/bloodCanticlesT1_cover.jpg" alt="Cover of Blood Witch, written by Naomi Clarke.  
Two white women sensually close to each other, in a graveyard background  
Published by Evernight Publishing" width="50%"/>

OK I know the cover is kinda clickbait and not really interesting (a tipical lesbian surnatural romance really) (others books of the author are worse :p), BUT it was a good read?

I was ranting about not finding enough queer main characters in urban fantasy, while all the discourse about monsters and outcasts (not the Wednesday kind ty) were pretty queer in itself, well, here it is! 

Some tension and murders, a bit of lesbian romance, blood magic, sultry demoness - please take my money!  
Tbh, not a huge twist of the genre, cops aren’t bastards here and it lack of other representations, but it was a fun read, and that's just what I was asking for :)

### The Big Book of Post-Collapse Fun - The Planetary Tarantella T01 (28/11/22)

**The Big Book of Post-Collapse Fun**, of Rachel Sharp.  VO : english.  
**CW / themes :** Post-apocalypse, Survivalism, Humor.

<img src="/assets/img/notes/reviews/thePlanetaryTarantellaT1_cover.jpg" alt="Cover of The Planetary Tarantella T1" width="50%"/>

I just finished The Big Book of Post-Collapse Fun, written by Rachel Sharp, part one of a trilogy, and it was really funny to read!

Post-apo isn't really a fun theme most of the time, but following Mag make it an almost joyful experience, even if she struggle a lot, make dubious decisions and has the survival skills of a chihuahua at the start of her journey. The humorous tone Mag use to tell her story make all the difference, and it's a pleasure to keep up on her adventure with her dog, in a geologically instable world who seems to have swallowed most of humanity.

I recommend it to all people sick of depressing takes of sci-fi where 'men are wolfs to men' is the norm. We may not be in an optimistic futur, but there is joy to find there still, and Mag and Vet (the dog) will find it along their treacherous way.

Can't wait to read the next book!

### Level 99 - Re-reading notes (29/12/21)

**You Thought You Wanted to Be Level 99, But Really You Wanted to Be a Better Person**, of [@Rowyn@mastodon.art](https://mastodon.art/@rowyn). Lang : english.  
**CW / themes :** LitRPG, Polyamour (M-F-F), MMO, love triangle.

<img src="/assets/img/notes/reviews/level99_cover.jpg" alt="Cover of Level 99" width="50%"/>

(it’s a reaaly good book, I don’t know why I didn’t really talked about it yet. Maybe I did, but didn’t tagged it or anything, so it’s lost in the aether. Please ask me about it!)

A thing I found really interesting in Level 99 is the way gender of the players differs from the gender of their characters, and the usage of both 'genders' both in the narration and between the players, or even in their inner thoughts.

Exemple :

> Grif was logging in to Guardian again. He'd spent most of his free time…

And :

> [Group] Razgathak: “Are you mocking her?”{talking about Jadaera, the female character controlled by Grif in the game}

Even if the players know themselves IRL, they still gender their characters accordingly, it have a roleplay function but I feel that it's more than that, it's also about respecting the identity the player choose to display?

Anyways, I really like the effect <3

For someone who is misgendered most of the time while I play on my old MMO, even when the players, sometime longtime friends, *know* I’m a trans woman…  
It would be so cool if it was the case in my old communities, but I don’t see it happening in my time.
