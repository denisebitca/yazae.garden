const markdownIt = require('markdown-it');
const markdownItFootnote = require('markdown-it-footnote');
const markdownItAttrs = require('markdown-it-attrs');
const anchor = require('markdown-it-anchor');
const markdownItTocDoneRight = require('markdown-it-toc-done-right');
const clean = require('eleventy-plugin-clean');
const syntaxHighlight = require('@11ty/eleventy-plugin-syntaxhighlight');
const pluginRss = require("@11ty/eleventy-plugin-rss");

const MARKDOWN_OPTIONS = {
  html: true,
  breaks: false,
  linkify: true,
};

module.exports = function(eleventyConfig) {
	
	// plugin to clean _site while building
	eleventyConfig.addPlugin(clean);

  // plugin to generate the RSS file
  eleventyConfig.addPlugin(pluginRss);

  const md = markdownIt(MARKDOWN_OPTIONS)
  .use(markdownItFootnote)
  .use(markdownItAttrs)
  .use(anchor, {
    permalink: anchor.permalink.headerLink()
  })
  .use(markdownItTocDoneRight)

  md.use(function(md) {
    // Recognize Mediawiki links ([[name | text]])
    md.linkify.add("[[", {
      validate: /^\s?([^\[\]\|\n\r]+)(\|[^\[\]\|\n\r]+)?\s?\]\]/,
      normalize: match => {
        const parts = match.raw.slice(2,-2).split("|");
        parts[0] = parts[0].replace(/.(md|markdown)\s?$/i, "");
        match.text = (parts[1] || parts[0]).trim();
        match.url = `/notes/${parts[0].trim()}`;
      }
    })
  })

  eleventyConfig.addFilter("markdownify", string => {
    return md.render(string)
  })

  eleventyConfig.setLibrary('md', md);

  eleventyConfig.addCollection("notes", function (collection) {
    return collection.getFilteredByGlob(["notes/*.md", "notes/Journal/*.md", "notes/Wiki/*.md", "notes/Drafts/*.md", "notes/Livres/*.md"])
    .sort((a, b) => b.data.date - a.data.date);
  });

  eleventyConfig.addCollection("feed", function(collectionApi) {
    // Use the `filter` method to create a filtered collection
    return collectionApi.getAll().filter(item => item.data.notetype === "feed")
    .sort((a, b) => b.data.date - a.data.date);
  });

  // Copy assets to `_site/`
  eleventyConfig.addPassthroughCopy('assets');
  // Copy `file` to `_site/`
  eleventyConfig.addPassthroughCopy({ "assets/img/favicon/favicon.ico": "/" });
  eleventyConfig.addPassthroughCopy({ "site.webmanifest.json": "/" });
  eleventyConfig.addPassthroughCopy({ "robots.txt": "/" });
  eleventyConfig.addPassthroughCopy({ ".domains": "/" });

  // 11ty will use .eleventyignore
  eleventyConfig.setUseGitIgnore(false);

  // // Enable syntax highlighting
  eleventyConfig.addPlugin(syntaxHighlight);

  // Reproduce some Liquid filters, sometimes losely
  eleventyConfig.addFilter('date_to_xmlschema', dateToXmlSchema)
  eleventyConfig.addFilter('date_to_rfc3339', dateToRFC3339)
  eleventyConfig.addFilter('group_by', groupBy)
  eleventyConfig.addFilter('number_of_words', numberOfWords)
  eleventyConfig.addFilter('sort_by', sortBy)
  eleventyConfig.addFilter('where', where)

  	// Return all the tags used in a collection
	eleventyConfig.addFilter("getAllTags", collection => {
		let tagSet = new Set();
		for(let item of collection) {
			(item.data.tags || []).forEach(tag => tagSet.add(tag));
		}
		return Array.from(tagSet);
	});

	eleventyConfig.addFilter("filterTagList", function filterTagList(tags) {
		return (tags || []).filter(tag => ["all", "nav", "post", "posts"].indexOf(tag) === -1);
	});

  return {
    markdownTemplateEngine: "liquid",
    htmlTemplateEngine: "liquid",
    dir: {
      input: "./",
      output: "_site",
      layouts: "layouts",
      includes: "includes",
      data: "_data"
    },
    passthroughFileCopy: true
  }
}

function dateToXmlSchema(value) {
  return new Date(value).toISOString()
}

function dateToRFC3339(value) {
  let date = new Date(value).toISOString()
  let chunks = date.split('.')
  chunks.pop()

  return chunks.join('') + 'Z'
}

// function dateToString(value) {
//   const date = new Date(value)
//   const formatter = new Intl.DateTimeFormat('en', {
//     year: 'numeric',
//     month: 'long',
//     day: '2-digit',
//   })
//   const parts = formatter.formatToParts(date)
//   const month = parts[0].value
//   const day = Number(parts[2].value)
//   const year = parts[4].value
//   const suffix = ['st', 'nd', 'rd'][day - 1] || 'th'

//   return month + ' ' + day + suffix + ', ' + year
// }

function groupBy(array, key) {
  const get = entry => key.split('.').reduce((acc, key) => acc[key], entry)

  const map = array.reduce((acc, entry) => {
    const value = get(entry)

    if (typeof acc[value] === 'undefined') {
      acc[value] = []
    }

    acc[value].push(entry)
    return acc
  }, {})

  return Object.keys(map).reduce(
    (acc, key) => [...acc, { name: key, items: map[key] }],
    []
  )
}

function numberOfWords(content) {
  return content.split(/\s+/g).length
}

function sortBy(array, key) {
  return array
    .slice(0)
    .sort((a, b) =>
      a[key].toLowerCase() < b[key].toLowerCase()
        ? -1
        : a[key].toLowerCase() > b[key].toLowerCase()
        ? 1
        : 0
    )
}

function where(array, key, value) {
  return array.filter(item => {
    const data = item && item.data ? item.data : item
    return typeof value === 'undefined' ? key in data : data[key] === value
  })
}

// function readingTime(content) {
//   return content
//     ? '~' +
//         Math.ceil(
//           (content.match(/[\u0400-\u04FF]+|\S+\s*/g) || []).length / 300
//         ) +
//         ' minutes'
//     : ''
// }