# Sass Guidelines
Auteur·e : [Kitty Giraudel](https://kittygiraudel.com/)

On va écrir ici les règles qui me semblent importantes à retenir, mais le document de référence sera toujours l'original, [Sass Guidelines](https://sass-guidelin.es/) (dispo en anglais et français). Version actuelle : 1.3.

## Syntaxe & formatage

- Une indentation à deux (2) espaces, pas de tabulation.
- Pas plus de 80 caractères par ligne si possible.
- Du CSS écrit sur plusieurs lignes.
- Une utilisation efficace des lignes vides.

```scss
// Yep
.foo {
  display: block;
  overflow: hidden;
  padding: 0 1em;
}

// Nope
.foo {
    display: block; overflow: hidden;

    padding: 0 1em;
}
```

### Strings

- Mettre les strings entre guillemets, `"string"` ou `'string'`.
- Ne concerne pas les identifiants CSS comme `inherit` ou les valeurs chiffrées.
- Concerne aussi les liens.
- Dans les faits, le code sera correct avec ou sans guillements autour des strings, mais c'est plus propre comme ça. Peut-être trouver un linter automatique ?

```scss
// Yep
$direction: 'left';
font-style: inherit;
font-size: 20em;
background-image: url('/img/kittens.jpg');

// Nope
$direction: left; // correct mais bof
font-style: 'inherit'; // mauvais code
font-size: '20em'; // mauvais code
background-image: url(/img/kittens.jpg); // correct mais bof
```




